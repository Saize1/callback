// Problem 1:

//     Using callbacks and the fs module's asynchronous functions, do the following:
//         1. Create a directory of random JSON files
//         2. Delete those files simultaneously

const fs = require("fs");
const path = require("path");
function problem1(directoryname, numberoffiles) {
  //creating a directory
  fs.mkdir(directoryname, (err) => {
    if (err) {
      console.log(err);
    } else {
      createfiles(directoryname, numberoffiles, (err, msg) => {
        if (err) {
          console.error(err);
        } else {
          //Reading the directory
          fs.readdir(directoryname, (err, data) => {
            if (err) {
              console.log(err);
            } else {
              // const files = data;
              // console.log(data);// In data all files or content are there
              removeFiles(directoryname, data, (err, data1) => {
                if (err) {
                  console.log(err);
                } else {
                  console.log(data1);
                }
              });
            }
          });
        }
      });
    }
  });
}
function createfiles(directoryname, numberoffiles, cb) {
  let count = 0;
  for (let idx = 1; idx <= numberoffiles; idx++) {
    const fileName = path.join(directoryname, `file${idx}.txt`); // join the file in the directory
    fs.writeFile(fileName, `Hello ${idx}`, (err) => {
      //creating a newfile
      if (err) {
        cb(err);
      } else {
        count++;
      }

      if (count === numberoffiles) {
        cb(null, "Done writing files");
      }
    });
  }
}
//delete all the files
function removeFiles(directoryname, files, cb) {
  let count = 0;
  const file1 = files.map((file) => {
    const fileName = path.join(directoryname, file);
    fs.unlink(fileName, (err) => {
      if (err) {
        cb(err);
      } else {
        count += 1;
        // console.log(count);
        if (count === files.length) {
          cb(null, "Done");
        }
        // console.log("File Deleted");
      }
    });

    // console.log(count, files.length);
  });
}
module.exports=problem1;


